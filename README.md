# AlterVision CPA Parking

**AlterCPA Parking** is parking server deployment script for AlterCPA based on [AlterCPA White](https://gitlab.com/altervision/altercpa-white).

**AlterCPA Паркинг** - это скрипт настройки сервера парковки доменов для AlterCPA на базе [AlterCPA White](https://gitlab.com/altervision/altercpa-white).

## Setting up

You'll need AlterCPA home domain and parking API-key to setup the server. Use them instead of `domain.com` and `parkingapikey`.

Для установки вам потребуется адрес домена, где установлена AlterCPA, и API-ключ парковки. Укажите их вместо `domain.com` и `parkingapikey`.

```bash
wget https://cpa.st/setup/park.sh
bash park.sh domain.com parkingapikey
```

## Developer

- WWW: https://www.altercpa.pro
- Email: me@wr.su
- Telegram: [@altercpapro](https://t.me/altercpapro)
- Skype: altervision13