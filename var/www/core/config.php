<?php

/*******************************************************************************

 *
 *  AlterVision CPA Parking
 *  Created by AlterVision - www.altercpa.pro
 *  Copyright (c) 2018-2020 Anton Reznichenko
 *

 *
 *  File:	core / config.php
 *  About:	System configuration
 *  Author:	Anton 'AlterVision' Reznichenko - altervision13@gmail.com
 *  URL:	https://gitlab.com/altervision/altercpa-park
 *

*******************************************************************************/

// Connection settings
define( 'ACPAURL', 'domain.ru' );
define( 'ACPAKEY', 'secret' );
define( 'ACPASSL', true );