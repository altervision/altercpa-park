#!/bin/bash

# Update the AlterCPA White main file
wget -q https://gitlab.com/altervision/altercpa-white/-/raw/master/index.php -O /var/www/core/white-new.php
if [ $? -eq 0 ]; then
	rm	/var/www/core/white.php
	mv /var/www/core/white-new.php /var/www/core/white.php
fi

# Clean up outdated cache
find /var/www/cache -type f -mtime +1 -delete
find /var/www/cache -type d -empty -delete
mkdir -p /var/www/cache
