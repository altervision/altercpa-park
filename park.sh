#!/bin/bash

# Usage:
# wget https://cpa.st/setup/park.sh
# bash park.sh <hostname.com> <parkapikey>

# Get the hostname from parameters
if [ -z "$1" ]; then
	echo "No AlterCPA domain specified"
	exit 1
else
	ACPAURL="$1"
fi

# Get the API key from parameters
if [ -z "$2" ]; then
	echo "No parking API key specified"
	exit 1
else
	ACPAKEY="$2"
fi

# Update system and install all the components
apt-get -y update
apt-get -y upgrade
apt-get -y install ca-certificates apt-transport-https
apt-get -y install coreutils mc nano net-tools curl php7.4-fpm nginx zip unzip
apt-get -y install php7.4-curl

# Prepare futher configuration
cd /root
PUBLICIP=`wget -qO- https://cpa.st/ip`
WWWPASS=`shuf -zer -n20 {A..Z} {a..z} {0..9} | tr -d "\r\n\0"`

# Make the result configuration file
echo "FTP" > config.txt
echo "" >> config.txt
echo "Address: sftp://$PUBLICIP:22" >> config.txt
echo "Login: wsvr" >> config.txt
echo "Password: $WWWPASS" >> config.txt
echo "Link: sftp://wsvr:$WWWPASS@$PUBLICIP:22" >> config.txt

# Make necessary directories
cd /root
mkdir -p /var/www
mkdir -p /var/www/acme
mkdir -p /var/www/data
mkdir -p /var/www/cache
rm -rf /var/www/html
mkdir -p /root/cert

# Create WWW user for FTP access
groupadd -g 1001 wsvr
useradd -g 1001 -u 1001 -d /var/www wsvr
echo -e "$WWWPASS\n$WWWPASS" | passwd wsvr

# Setup PHP modules
phpdismod calendar
phpdismod ctype
phpdismod exif
phpdismod fileinfo
phpdismod ftp
phpdismod gettext
phpdismod iconv
phpdismod phar
phpdismod pdo
phpdismod posix
phpdismod readline
phpdismod shmop
phpdismod sockets
phpdismod sysvmsg
phpdismod sysvsem
phpdismod sysvshm
phpdismod tokenizer

# Load configuration archive
ufw disable
wget -q https://cpa.st/setup/debian-park.zip
unzip -o -qq debian-park.zip -d /

# Change change passwords in configuration files
sed -i "s/domain.ru/$ACPAURL/g" /var/www/core/config.php
sed -i "s/secret/$ACPAKEY/g" /var/www/core/config.php

# Change file permissions
chmod a+x /root/acme/dehydrated
chmod a+x /root/webssl
chown -R wsvr:wsvr /var/www

# Restart all the services
service nginx restart
service php7.4-fpm restart

# Download content
wget -q https://gitlab.com/altervision/altercpa-white/-/raw/master/index.php -O /var/www/core/white.php
/root/acme/dehydrated --config /root/acme/config --register --accept-terms
nohup openssl dhparam -out /etc/nginx/ssl/dhparam.pem 2048 &

# Root cron processor
echo "*/30 * * * * bash /root/webssl >/dev/null 2>&1" > /var/spool/cron/crontabs/root
crontab /var/spool/cron/crontabs/root

# User cron processor
echo "* * * * * php -f /var/www/core/cron.php >/dev/null 2>&1" > /var/spool/cron/crontabs/wsvr
echo "1 * * * * bash /var/www/core/white.sh >/dev/null 2>&1" >> /var/spool/cron/crontabs/wsvr
crontab -u wsvr /var/spool/cron/crontabs/wsvr
