<?php

/*******************************************************************************

 *
 *  AlterVision CPA Parking
 *  Created by AlterVision - www.altercpa.pro
 *  Copyright (c) 2018-2020 Anton Reznichenko
 *

 *
 *  File:	core / cron.php
 *  About:	Domain list updater
 *  Author:	Anton 'AlterVision' Reznichenko - altervision13@gmail.com
 *  URL:	https://gitlab.com/altervision/altercpa-park
 *

*******************************************************************************/

// Prepare system configuration
require_once 'config.php';
if ( strpos( ACPAKEY, '-' ) !== false ) {
	$api = ( defined('ACPASSL') ? 'https://' : 'http://' ) . ACPAURL . '/api/wm/park.json?id=' . ACPAKEY;
} else $api = ( defined('ACPASSL') ? 'https://' : 'http://' ) . ACPAURL . '/api/site/park.json?key=' . ACPAKEY;

// Get the domains list
$curl = curl_init( $api );
curl_setopt( $curl, CURLOPT_RETURNTRANSFER, 1 );
curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, 1 );
curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, 0 );
curl_setopt( $curl, CURLOPT_SSL_VERIFYHOST, 0 );
curl_setopt( $curl, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64; rv:63.0) Gecko/20100101 Firefox/63.0' );
$result = curl_exec( $curl );
curl_close( $curl );

// Domain lists
$dml = 'abcdefghijklmnpoqrstuvwxyz1234567890';
$domain = $domains = [];
for ( $i = 0; $i < 36; $i++ ) {
	$di = $dml{$i};
	$domain["tld-$di"] = [];
	$domain["idn-$di"] = [];
}

// Parse the domains list
$dls = json_decode( $result, true );
if ( $dls['status'] != 'ok' ) die();
foreach ( $dls['domain'] as $d => $u ) {

	// Choose the domain listing to use
	if ( substr( $d, 0, 4 ) == 'xn--' ) {
		$di = $d{4};
		$dk = "idn-$di";
	} else {
		$di = $d{0};
		$dk = "tld-$di";
	}

	// Simply add URL to the list
	$domain[$dk][$d] = $u;
	$domains[] = $d;

}

// Save the files
$modified = false;
$dd5 = json_decode( file_get_contents( '/var/www/data/md5.json' ), true );
foreach ( $domain as $f => $l ) {

	// Make the file contents
	ksort( $l );
	$xprt = var_export( $l, true );
	$data = "<?php\nreturn $xprt;\n?>";

	// Check the file MD5
	$mdf = md5( $data );
	if ( $mdf != $dd5[$f] ) {
		$modified = true;
		$dd5[$f] = $mdf;
		$fp = "/var/www/data/$f.php";
		file_put_contents( $fp, $data, LOCK_EX );
		opcache_invalidate( $fp, true );
	}

}

// Save modified info
if ( $modified ) {
	file_put_contents( '/var/www/data/md5.json', json_encode( $dd5, JSON_PRETTY_PRINT ) );
	sort( $domains );
	$dlt = implode( "\n", $domains );
	file_put_contents( '/var/www/data/domains.txt', "$dlt\n" );
}

// end. ;)