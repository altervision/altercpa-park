<?php

/*******************************************************************************

 *
 *  AlterVision CPA Parking
 *  Created by AlterVision - www.altercpa.pro
 *  Copyright (c) 2018-2020 Anton Reznichenko
 *

 *
 *  File:	web / connect-imklo.php
 *  About:	IM KLO smartlink connector
 *  Author:	Anton 'AlterVision' Reznichenko - altervision13@gmail.com
 *  URL:	https://gitlab.com/altervision/altercpa-park
 *

*******************************************************************************/

// Bad request
function badrequest() {
	http_response_code( 404 );
	die();
}

// Make the redirect
function go( $url ) {
	header( "Location: $url" );
	die();
}

// Get site info from IM KLO
if (isset( $_GET['domain'] )) {

	// Work with selected domain
	$domain = filter_var( $_GET['domain'], FILTER_SANITIZE_URL );

	// Create the request
	$post['ip'] = $_SERVER['HTTP_CLIENT_IP'];
	$post['domain'] = $_SERVER['HTTP_HOST'];
	$post['referer'] = @$_SERVER['HTTP_REFERER'];
	$post['user_agent'] = $_SERVER['HTTP_USER_AGENT'];
	$post['url'] = $_SERVER['REQUEST_URI'];
	$post['headers'] = json_encode(apache_request_headers());

	// Get IP info
	$curl = curl_init( 'http://'.$domain.'/api/check_ip' );
	curl_setopt( $curl, CURLOPT_RETURNTRANSFER, true );
	curl_setopt( $curl, CURLOPT_FOLLOWLOCATION, true );
	curl_setopt( $curl, CURLOPT_SSL_VERIFYPEER, false );
	curl_setopt( $curl, CURLOPT_TIMEOUT, 60 );
	curl_setopt( $curl, CURLOPT_POST, true );
	curl_setopt( $curl, CURLOPT_POSTFIELDS, $post );
	$result = curl_exec( $curl );
	curl_close( $curl );

	// Check the result
	if ( ! $result ) badrequest();
	$info = json_decode( $result, true );
	if ( $info['link'] ) go( $info['link'] );
	if ( $info['white_link'] ) go( $info['white_link'] );
	badrequest();

} else badrequest();