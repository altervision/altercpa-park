<?php

/*******************************************************************************

 *
 *  AlterVision CPA Parking
 *  Created by AlterVision - www.altercpa.pro
 *  Copyright (c) 2018-2020 Anton Reznichenko
 *

 *
 *  File:	web / index.php
 *  About:	Main parking processor
 *  Author:	Anton 'AlterVision' Reznichenko - altervision13@gmail.com
 *  URL:	https://gitlab.com/altervision/altercpa-park
 *

*******************************************************************************/

// Get the hostname
$domain = $_SERVER['HTTP_HOST'];
$domain = strtolower( $domain );
if ( substr( $domain, 0, 4 ) == 'www.' ) $domain = substr( $domain, 4 );

// Checking procedure
if (isset( $_POST['checkit'] )) {
	require_once '/var/www/core/config.php';
	$ck = hash_hmac( 'sha256', $domain, ACPAKEY );
	if ( $_POST['checkit'] == $ck ) die( 'ok' );
}

// Find the file to work with
if ( substr( $domain, 0, 4 ) == 'xn--' ) {
	$di = $domain{4};
	$df = "/var/www/data/idn-$di.php";
} else {
	$di = $domain{0};
	$df = "/var/www/data/tld-$di.php";
}

// Everything gone bad
function popizde() {
	http_response_code( 500 );
	die();
}

// Get the URL
if (!file_exists( $df )) popizde();
$urls = require_once $df;
if ( ! $urls[$domain] ) popizde();

// Check for cloak and set the URL
$uxp = parse_url( $urls[$domain], PHP_URL_PATH );
if (preg_match( '#^\/clo\/([0-9]+)\-([\w]+)\-([0-9]+)$#i', $uxp )) {
	define( 'CLOAK', $urls[$domain] );
} elseif (preg_match( '#^\/fltr\/([0-9]+)\-([\w]+)\-([0-9]+)$#i', $uxp )) {
	define( 'CLOAK', $urls[$domain] );
} else define( 'SMARTLINK', $urls[$domain] );

// Set the parameters and go to White
define( 'CACHE', true );
define( 'CACHEPATH', '/var/www/cache' );
require_once '/var/www/core/white.php';

// end. =D